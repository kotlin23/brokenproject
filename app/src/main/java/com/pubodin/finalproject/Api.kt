package com.pubodin.finalproject

import com.pubodin.finalproject.model.BooksItem
import retrofit2.Call
import retrofit2.http.GET

interface Api {
    @GET("books")
    fun getData(): Call<List<BooksItem>>
}