package com.pubodin.finalproject

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pubodin.finalproject.databinding.FragmentHomeBinding
import com.pubodin.finalproject.model.BooksItem
import com.squareup.picasso.Picasso
import org.w3c.dom.Text
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Home.newInstance] factory method to
 * create an instance of this fragment.
 */
const val BASE_URL = "http://10.0.2.2:3000"
class Home : Fragment() {
    private var dataList: MutableList<BooksItem> = mutableListOf()
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //getMyDataApi()
        getData()
        val recyclerView = binding.recyclerView
        recyclerView.adapter = ItemAdapter(dataList,requireContext())
        var adapters = ItemAdapter(dataList,requireContext())
        recyclerView.adapter = adapters
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        adapters.setOnItemClickListener(object : ItemAdapter.onItemClickListener {
            override fun onItemClick(position: Int) {
                Toast.makeText(
                    context,
                    dataList[position].price.toString() + "     " + dataList[position].titleName,
                    Toast.LENGTH_LONG
                ).show()
            }
        })
    }

    private fun getMyDataApi() {
        Log.d("CheckApi","Start get Data")
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(Api::class.java)
        Log.d("CheckApi","Retrofit2")
        val retrofitData = retrofitBuilder.getData()
        Log.d("CheckApi","Start enqueue")
        retrofitData.enqueue(object : Callback<List<BooksItem>?> {
            override fun onResponse(
                call: Call<List<BooksItem>?>,
                response: Response<List<BooksItem>?>
            ) {
                if (response.code() == 200) {
                    Log.d("CheckApi","pass")
                }else{
                    Log.d("CheckApi","fail")
                }
                dataList = response.body() as MutableList<BooksItem>
            }

            override fun onFailure(call: Call<List<BooksItem>?>, t: Throwable) {
                Log.d("CheckApi","fail")
            }
        })
    }
    private fun getData(){
        dataList.add(BooksItem("BURNING HELL นครแห่งพระเจ้า (เล่มเดียวจบ)","YANG KYUNG-IL","Cartoon","ในประเทศที่ไม่เหลือซึ่งความดีความชั่ว เพราะสงครามและความอดอยากที่ดำเนินมาอย่างยาวนาน มือสังหารได้จัดการฆ่าทหารองครักษ์ส่วนองค์ชาย อี มุน ที่รอดชีวิตมาได้เพียงคนเดียว ก็ใช้เงินจ้างโจรภูเขานามว่าจูฮาแล้วหลบหนีไปด้วยกัน นี่คือเรื่องราวการผจญภัย แฟนตาซีขององค์ชายกับโจรภูเขา!!",185,"https://inwfile.com/s-du/indlip.jpg","https://www.google.co.th/search?q=BURNING+HELL+%E0%B8%99%E0%B8%84%E0%B8%A3%E0%B9%81%E0%B8%AB%E0%B9%88%E0%B8%87%E0%B8%9E%E0%B8%A3%E0%B8%B0%E0%B9%80%E0%B8%88%E0%B9%89%E0%B8%B2+%28%E0%B9%80%E0%B8%A5%E0%B9%88%E0%B8%A1%E0%B9%80%E0%B8%94%E0%B8%B5%E0%B8%A2%E0%B8%A7%E0%B8%88%E0%B8%9A%29",0))
        dataList.add(BooksItem("World Ghost","Yim, Kang-jae","Cartoon","พบกับเรื่องเล่าสุดหลอนจากโรงเรียนทั่วโลก ตำนานภูคผีปีศาจและสิ่งลึกลับที่เล่าขานกันมาจากรุ่นสู่รุ่น",175,"https://storage.naiin.com/system/application/bookstore/resource/product/202201/541131/1000246217_front_XL.jpg?t=au&imgname=%E0%B8%9C%E0%B8%B5%E0%B8%97%E0%B8%B1%E0%B9%88%E0%B8%A7%E0%B9%82%E0%B8%A5%E0%B8%81-%E0%B9%80%E0%B8%A3%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%87%E0%B8%9C%E0%B8%B5%E0%B9%86-%E0%B8%A3%E0%B8%AD%E0%B8%9A%E0%B9%82%E0%B8%A5%E0%B8%81","https://www.google.co.th/search?q=World+Ghost+Yim%2C+Kang-jae",0))
        dataList.add(BooksItem("DRAGON BALL SUPER","Akira Toriyama","Cartoon", "การต่อสู้ระหว่างกองทหารของโมโรที่มายังโลกกับเหล่านักสู้ดาวโลกได้เริ่มปะทุขึ้น! ในที่สุดโมโรก็ลงมายังพื้นโลก พวกโงฮังต้องสู้กับซากัมโบ้ที่โมโรเพิ่มพลังให้อย่างยากลำบาก โงคูกับเบจิต้าจะมาช่วยโลกที่กำลังวิกฤติได้ทันหรือไม่!?",175,"https://cdn-shop.ookbee.com/Books/NEDCLUB_CODE/2020/20200811071554/Thumbnails/Cover.jpg","https://www.google.co.th/search?q=DRAGON+BALL+SUPER+Akira+Toriyama",0))
        dataList.add(BooksItem("MY HERO ACADEMIA","Kohei Horikoshi","Cartoon","นอกจากจะปล่อยให้ชิงาราคิหนีไปได้ ความเสียหายยังหนักหนานักแต่ว่า...ถึงอย่างนั้นพวกเราฮีโร่ก็ยังมีปณิธานแน่วแน่เป็นหนึ่งเดียวว่าจะต้องต่อสู้ต่อไปให้ได้!",70,"https://pbs.twimg.com/media/EbbbFmIUwAESYCA?format=jpg&name=4096x4096","https://www.google.co.th/search?q=MY+HERO+ACADEMIA+Kohei+Horikoshi",0))
        dataList.add(BooksItem("WAKE UP WITH THE KISS","NANA HARUTA","Cartoon","ใครจะไปชอบน้องชายแก่แดดแบบนั้น...กันล่ะ การพบพานนี้คือโชคชะตา? หรือจะเป็นเรื่องต้องห้ามกันแน่...!?เรื่องราวความรักของพี่น้องบุญธรรมได้เริ่มต้นขึ้นแล้วเอนะผู้มีน้องชาย น้องสาว บุญธรรมเพิ่มขึ้นมาจากการแต่งงานใหม่ของแม่ แต่ โทมะ น้องชายบุญธรรมที่เป็นพื่อนร่วมชั้นกลับพูดอย่างไร้เยื่อใยว่า ห้ามชอบฉันเด็ดขาดเลยนะ?",95,"https://inwfile.com/s-du/wgmcc1.png","https://www.google.co.th/search?q=WAKE+UP+WITH+THE+KISS+NANA+HARUTA",0))
    }

    class ItemAdapter(private val dataList: MutableList<BooksItem>,private val context: Context) : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {

        private lateinit var mListener: onItemClickListener

        interface onItemClickListener {
            fun onItemClick(position: Int)
        }

        fun setOnItemClickListener(listener: onItemClickListener) {
            mListener = listener
        }


        class ViewHolder(private val view : View, listener: onItemClickListener) : RecyclerView.ViewHolder(view){
            val titleName: TextView = view.findViewById(R.id.bookName)
            val price: TextView = view.findViewById(R.id.price)
            val picture : ImageView = view.findViewById(R.id.picture)
            val type : TextView = view.findViewById(R.id.type)
            init {
                itemView.setOnClickListener {
                    listener.onItemClick(adapterPosition)
                }
            }

        }
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val adapterLayout = LayoutInflater.from(parent.context).inflate(R.layout.item_view, parent,false)
            return ViewHolder(adapterLayout, mListener)
        }


        override fun getItemCount(): Int {
            return dataList.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val data = dataList[position]
            val titleName = holder.titleName
            val price = holder.price
            val picture = holder.picture
            val type = holder.type
            titleName.text = data.titleName
            price.text = data.price.toString()+"฿"
            type.text = data.type
            Picasso.get()
                .load(data.image)
                .into(picture)
        }


    }



}