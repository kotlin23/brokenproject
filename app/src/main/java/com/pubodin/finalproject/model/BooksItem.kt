package com.pubodin.finalproject.model

import com.google.gson.annotations.SerializedName

data class BooksItem(
    val titleName: String,
    val author: String,
    val type: String,
    val des: String,
    val price: Int,
    val image: String,
    val url: String,
    val fav: Int
)